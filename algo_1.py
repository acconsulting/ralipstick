# Import Module
import cv2
import requests
import time, threading
from PIL import Image
from PIL import ImageColor
import os
import numpy as np
from skimage.filters import gaussian
from repo import evaluate
# import argparse
# import warnings
# warnings.filterwarnings("ignore")

counter = 0

    
StartTime=time.time()

def sharpen(img):
    img = img * 1.0
    gauss_out = gaussian(img, sigma=5, multichannel=True)

    alpha = 1.5
    img_out = (img - gauss_out) * alpha + img

    img_out = img_out / 255.0

    mask_1 = img_out < 0
    mask_2 = img_out > 1

    img_out = img_out * (1 - mask_1)
    img_out = img_out * (1 - mask_2) + mask_2
    img_out = np.clip(img_out, 0, 1)
    img_out = img_out * 255
    return np.array(img_out, dtype=np.uint8)

def viso(image, parsing, part=17, color=[0, 0, 0]):
    b, g, r = color      #[10, 50, 250]       # [10, 250, 10]
    tar_color = np.zeros_like(image)
    tar_color[:, :, 0] = b
    tar_color[:, :, 1] = g
    tar_color[:, :, 2] = r

    #     50566d
    #     rgb(80, 86, 109)
    #     coloreLipstick = ImageColor.getcolor('#50566d', "RGB") B G R
    # 52 per prevalenza blue   cv2.COLOR_BGR2HLS
    # 40 per prevalenza red    cv2.COLOR_BGR2HSV

    codiceColore = cv2.COLOR_BGR2HSV
    if b > r:
        codiceColore = cv2.COLOR_BGR2HLS
        
    image_hsv = cv2.cvtColor(image, codiceColore) 
    tar_hsv = cv2.cvtColor(tar_color,codiceColore )

    if part == 12 or part == 13:
        image_hsv[:, :, 0:2] = tar_hsv[:, :, 0:2]
    else:
        image_hsv[:, :, 0:1] = tar_hsv[:, :, 0:1]

    image_hsv[:, :, 0:1] = tar_hsv[:, :, 0:1]
    
    changed = cv2.cvtColor(image_hsv, cv2.COLOR_HSV2BGR)

    if part == 17:
        changed = sharpen(changed)

    changed[parsing != part] = image[parsing != part]
    return changed

def action() :
    global counter
    global url_upload
    global url_job_text

    counter = counter + 1
    print('action-bubbino ! -> time : {:.1f}s'.format(time.time()-StartTime))
    print(counter)

    try:   
        immagini=[]
        response = requests.get(url_job_text)
        data = response.text
        del response
        for i, line in enumerate(data.split('\n')):
            immagini.append(f'{line}')

        numeroImmagini = len(immagini)
        if numeroImmagini > 100:
            if immagini[0].find('html') != -1:
                print("File job mancante!")
            else:
                for immagine in immagini:
                    elabora_immagine(immagine)

        else:
            if(not len(immagini[0]) == 0):
                try:
                    # upload file job vuoto
                    files = {'upload_job': open('./immagini/job.txt','rb')}
                    values = {'metadata': ''}
                    response = requests.post(url_upload, files=files, data=values)
                except Exception as err:
                    print ("error [" + str(err) + "]") 

                for immagine in immagini:
                    elabora_immagine(immagine)
                print( immagini[0])

            else :
                print ("nessuna immagine")

        if( counter == 500 ):
            counter = 0
            time.sleep(10)
    except Exception as err:
        print ("error [" + str(err) + "]") 
        


def elabora_immagine( immagine ) :
    # Fill Required Information
    global url_upload
    global url_job_text
    global url_folder
    
    pathname = immagine.split("/")[-1]
    filename, extension = os.path.splitext(pathname)

    colore = "#" + filename.split("_")[-1]
    if filename != '':
#         print(filename + " " +extension + " " + colore)
#         print(ImageColor.getcolor(colore, "RGB"))
        coloreLipstick = ImageColor.getcolor(colore, "RGB")

        filename_mod = filename + "_mod" + extension
        img = Image.open(requests.get(url_folder + immagine, stream = True).raw)
        img.save("./immagini/" + filename_mod )

        table = {
            'upper_lip': 12,
            'lower_lip': 13
        }

        image_path = "./immagini/" + filename_mod 
        cp = 'cp/79999_iter.pth'
        image = cv2.imread(image_path)
        ori = image.copy()
        parsing = evaluate(image_path, cp)
        parsing = cv2.resize(parsing, image.shape[0:2], interpolation=cv2.INTER_NEAREST)
        parts = [table['upper_lip'], table['lower_lip']]
        colors = [ [coloreLipstick[2], coloreLipstick[1], coloreLipstick[0]], [coloreLipstick[2], coloreLipstick[1], coloreLipstick[0]] ]
        for part, color in zip(parts, colors):
            image = viso(image, parsing, part, color)
        cv2.resize(image,(512,512))
        cv2.imwrite('./immagini_elaborate/' + filename_mod, image)
        try:
            # upload immagine elaborata
            files = {'upload_job': open("./immagini_elaborate/" + filename_mod,'rb')}
            values = {'metadata': ''}
            response = requests.post(url_upload, files=files, data=values)
        except Exception as err:
            print ("error [" + str(err) + "]") 

#     os._exit(0)


class setInterval :
    def __init__(self,interval,action) :
        self.interval=interval
        self.action=action
        self.stopEvent=threading.Event()
        thread=threading.Thread(target=self.__setInterval)
        thread.start()

    def __setInterval(self) :
        nextTime=time.time()+self.interval
        while not self.stopEvent.wait(nextTime-time.time()) :
            nextTime+=self.interval
            self.action()

    def cancel(self) :
        self.stopEvent.set()



inter=setInterval(1.5,action)
print('just after setInterval -> time : {:.1f}s'.format(time.time()-StartTime))
t=threading.Timer(360000,inter.cancel)
t.start()
